[![Docker Image](https://img.shields.io/badge/docker%20image-available-green.svg)](https://hub.docker.com/r/elleflorio/svn-server/)

# 描述
1. fork [https://github.com/elleFlorio/svn-docker.git](https://github.com/elleFlorio/svn-docker.git)
2. 原始镜像无法和 ldap 集成

# 修改内容
1. dav_svn.conf 增加 ldap配置
2. Dockerfile 增加 apache2_ldap 安装
3. iF.SVNAdmin-stable-1.6.2.zip 放到目录内，解决链接超时
4. 修改为科大镜像

#  Running Commands

``` shell
# 创建镜像
docker build -t xxxx/svn-server:v1.0.0 .
```





